REGISTRY = registry.gitlab.com
IMAGE_NAME = sidechef/aws-cli
IMAGE = ${REGISTRY}/${IMAGE_NAME}


build:
	docker build -t ${IMAGE} .

run:
	docker run --rm -it --entrypoint /bin/sh ${IMAGE}

push:
	docker push ${IMAGE}
