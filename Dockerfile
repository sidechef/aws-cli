FROM --platform=linux/amd64 alpine
RUN apk update && apk add --update --no-cache python3 py3-pip aws-cli git
RUN aws --version
